package algorismProject2.tree;

public class BinaryNode<K, V> {
	private BinaryTree<K, V> tree;
	private BinaryNode<K, V> left;
	private BinaryNode<K, V> right;
	
	public final K KEY;
	public final V VALUE;
	
	BinaryNode(K key, V value) {
		this.KEY = key;
		this.VALUE = value;
	}
	
	BinaryNode(K key, V value, BinaryTree<K, V> tree) {
		this.KEY = key;
		this.VALUE = value;
		this.tree = tree;
	}

	public BinaryTree<K, V> getTree() {
		return tree;
	}

	public BinaryNode<K, V> getLeft() {
		return left;
	}

	public BinaryNode<K, V> getRight() {
		return right;
	}
	
	void setTree(BinaryTree<K, V> tree) {
		if(this.tree != null) {
			return;
		}
		this.tree = tree;
	}
	
	void setLeft(BinaryNode<K, V> node) {
		if(tree == null) {
			return;
		}
		left = node;
	}

	void setRight(BinaryNode<K, V> node) {
		if(tree == null) {
			return;
		}
		right = node;
	}

	void invalidate() {
		tree = null;
		left = null;
		right = null;
	}
}
