package algorismProject2.tree;

import java.util.Comparator;

public class BinaryTree<K, V> {
	private final Comparator<K> keyComparator;
	
	private BinaryNode<K, V> root = null;
	
	public BinaryTree(Comparator<K> keyComparator) throws IllegalArgumentException {
		if(keyComparator == null) {
			throw new IllegalArgumentException("'comparator' cannot be null");
		}
		this.keyComparator = keyComparator;
	}
	
	public BinaryNode<K, V> getRoot() {
		return root;
	}
	
	public void put(K key, V value) {
		BinaryNode<K, V> newNode = new BinaryNode<K, V>(key, value);
		newNode.setTree(this);
		
		if(root == null) {
			root = newNode;
			return;
		}
		
		BinaryNode<K, V> parent = null;
		BinaryNode<K, V> current = root;
		while(true) {
			int c = keyComparator.compare(key, current.KEY);
			if(c == 0) {
				// 중복된 키값이 있을 경우 기존 노드를 대체한다.
				if(current == root) {
					root = newNode;
				} else if(parent.getLeft() == current) {
					parent.setLeft(newNode);
				} else {
					parent.setRight(newNode);
				}
				newNode.setLeft(current.getLeft());
				newNode.setRight(current.getRight());
				
				// 기존 노드를 무효화 시킨다.
				current.invalidate();
				return;
			} else if (c > 0) {
				BinaryNode<K, V> right = current.getRight();
				if(right == null) {
					current.setRight(newNode);
					return;
				} else {
					parent = current;
					current = right;
				}
			} else {
				BinaryNode<K, V> left = current.getLeft();
				if(left == null) {
					current.setLeft(newNode);
					return;
				} else {
					parent = current;
					current = left;
				}
			}	
		}
	}
	
	public boolean remove(K key) {
		BinaryNode<K, V> parentOfTarget = null; // 삭제할 타겟의 부모
		BinaryNode<K, V> removeTarget = root; // 삭제할 타겟을 넣을 변수
		
		// removeTarget 탐색
		while(true) {
			if(removeTarget == null) {
				return false;
			}
			int c = keyComparator.compare(key, removeTarget.KEY);
			if(c == 0) {
				break;
			} else {
				parentOfTarget = removeTarget;
				if(c > 0) {
					removeTarget = removeTarget.getRight();
				} else {
					removeTarget = removeTarget.getLeft();
				}
			}
		}
		
		BinaryNode<K, V> leftOfTarget = removeTarget.getLeft();
		BinaryNode<K, V> rightOfTarget = removeTarget.getRight();
		removeTarget.invalidate();
		if(leftOfTarget == null) {
			if(removeTarget == root) {
				root = rightOfTarget;
			} else if(parentOfTarget.getLeft() == removeTarget) {
				parentOfTarget.setLeft(rightOfTarget);
			} else {
				parentOfTarget.setRight(rightOfTarget);
			}
		} else if(rightOfTarget == null) {
			if(removeTarget == root) {
				root = leftOfTarget;
			} else if(parentOfTarget.getLeft() == removeTarget) {
				parentOfTarget.setLeft(leftOfTarget);
			} else {
				parentOfTarget.setRight(leftOfTarget);
			}
		} else {
			// 삭제할 타겟의 Left, Right 자식이 모두 있을 경우
			// 좌측의 가장 큰 노드를 삭제하는 노드의 위치로 이동시킨다.
			BinaryNode<K, V> parentOfMaxOfLeft = removeTarget;
			BinaryNode<K, V> maxOfLeft = leftOfTarget;
			while(true) {
				BinaryNode<K, V> right = maxOfLeft.getRight();
				if(right == null) {
					break;
				}
				parentOfMaxOfLeft = maxOfLeft;
				maxOfLeft = right;
			}
			
			// maxOfLeft를 그 부모에서 제거함
			if(maxOfLeft == leftOfTarget) {
				parentOfMaxOfLeft.setLeft(maxOfLeft.getLeft());
			} else {
				parentOfMaxOfLeft.setRight(maxOfLeft.getLeft());
			}
			
			// maxOfLeft를 removeTarget에 끼워넣음
			if(removeTarget == root) {
				root = maxOfLeft;
			} else if(parentOfTarget.getLeft() == removeTarget) {
				parentOfTarget.setLeft(maxOfLeft);
			} else {
				parentOfTarget.setRight(maxOfLeft);
			}
			
			// removeTarget의 자식을 maxOfLeft의 자식으로 이동
			maxOfLeft.setLeft(leftOfTarget);
			maxOfLeft.setRight(rightOfTarget);
		}
		return true;
	}
	
	public BinaryNode<K, V> getNode(K key) {
		BinaryNode<K, V> pointer = root;
		
		while(true) {
			if(pointer == null) {
				return null;
			}
			int c = keyComparator.compare(key, pointer.KEY);
			if(c == 0) {
				return pointer;
			} else if(c > 0) {
				pointer = pointer.getRight();
			} else {
				pointer = pointer.getLeft();
			}
		}
	}
	
	public V get(K key) {
		return get(key, null);
	}
	
	public V get(K key, V def) {
		BinaryNode<K, V> node = getNode(key);
		return node == null ? def : node.VALUE;
	}
	
	public boolean containsKey(K key) {
		return getNode(key) != null;
	}
}
