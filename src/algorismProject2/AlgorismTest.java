package algorismProject2;

import java.util.Comparator;
import java.util.Scanner;

import algorismProject2.tree.BinaryNode;
import algorismProject2.tree.BinaryTree;

public class AlgorismTest {

	private enum Menu {
		ADD("상품 등록"),
		REMOVE("상품 삭제"),
		SEARCH("상품 검색"),
		PRINT("전체 상품 조회"),
		EXIT("종료");
		
		public final String message;
		
		static Menu MenuAt(int idx) {
			Menu[] menus = Menu.values();
			if(idx >= 0 && idx < menus.length) {
				return menus[idx];
			}
			return null;
		}
		
		Menu(String msg) {
			message = msg;
		}
	}
	
	private static BinaryTree<Integer, String> btree = new BinaryTree<Integer, String>(new Comparator<Integer>() {
		@Override
		public int compare(Integer o1, Integer o2) {
			return o1 - o2;
		}
	});
	
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		Menu menu = null;
		
		do {
			switch(menu = selectMenu()) {
			case ADD :
				addProduct();
				break;
			case REMOVE :
				removeProduct();
				break;
			case SEARCH :
				searchProduct();
				break;
			case PRINT :
				print();
				break;
			}
		} while(menu != Menu.EXIT);
		
		scanner.close();
	}
	
	private static Menu selectMenu() {
		for(Menu menu : Menu.values()) {
			System.out.printf("(%d) %s ", menu.ordinal() + 1, menu.message);
		}
		System.out.println();
		
		Menu menu = null;
		do {
			System.out.print("메뉴 선택: ");
			try {
				menu = Menu.MenuAt(Integer.parseInt(scanner.nextLine()) - 1);
			} catch (NumberFormatException ignore) {}
		} while(menu == null);
		System.out.println();
		
		return menu;
	}
	
	private static void addProduct() {
		System.out.println(Menu.ADD.message);

		int num;
		String name;
		do {
			System.out.print("상품 번호 입력: ");
			try {
				num = Integer.parseInt(scanner.nextLine());
				break;
			} catch (NumberFormatException ignore) {}
		} while(true);
		
		System.out.print("상품명 입력: ");
		name = scanner.nextLine();
		System.out.println();
		
		btree.put(num, name);
	}
	
	private static void removeProduct() {
		System.out.println(Menu.REMOVE.message);

		int num;
		do {
			System.out.print("상품 번호 입력: ");
			try {
				num = Integer.parseInt(scanner.nextLine());
				break;
			} catch (NumberFormatException ignore) {}
		} while(true);

		if(btree.remove(num)) {
			System.out.println("상품 삭제 완료");
		} else {
			System.out.println("일치하는 상품 없음");
		}
		System.out.println();
	}
	
	private static void searchProduct() {
		System.out.println(Menu.SEARCH.message);

		int num;
		do {
			System.out.print("상품 번호 입력: ");
			try {
				num = Integer.parseInt(scanner.nextLine());
				break;
			} catch (NumberFormatException ignore) {}
		} while(true);

		String name = btree.get(num, null);
		if(name != null) {
			System.out.printf("상품명 : %s\n", name);
		} else {
			System.out.println("일치하는 상품 없음");
		}
		System.out.println();
	}
	
	private static void print() {
		BinaryNode<Integer, String> root = btree.getRoot();
		if(root != null) {
			printSubTree(root);			
		} else {
			System.out.println("등록된 상품이 없습니다.");
		}
		System.out.println();
	}
	
	private static void printSubTree(BinaryNode<Integer, String> subRoot) {
		if(subRoot != null) {
			printSubTree(subRoot.getLeft());
			System.out.printf("%d %s\n", subRoot.KEY, subRoot.VALUE);
			printSubTree(subRoot.getRight());
		}
	}
}
